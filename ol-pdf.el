;;; ol-pdf.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Rohan Goyal
;;
;; Author: Rohan Goyal  <https://github.com/rohan>
;; Maintainer: Rohan Goyal  <goyal.rohan.03@gmail.com>
;; Created: December 26, 2021
;; Modified: December 26, 2021
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/rohan/ol-pdf
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:
(require 'ol)

(org-link-set-parameters "pdf+page")


(provide 'ol-pdf)
;;; ol-pdf.el ends here
